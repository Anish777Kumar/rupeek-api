**PROJECT DETAILS**
```
* "api.payload" package contains Endpoints and Payload for different test cases
* "api.payloads.helper" contains Headers for different test cases
* "api.test" contains Test cases for the API
* "api.utils" contains wrapper of the required HTTP methods
* "common.utils" contains Logger to write logs
* "report" contains helper class to generate report
* "tools" contains helpers required in our test cases , like data-provider,basetest etc
* "src/test/resources" contains the test data in a json file , where the name of the file must be same as the testcase name followed by ".json" as file extension
```

**#Execution** 
```
	PreRequisites
	* Install Maven 3.5 or above
    *Install Java 8

	
Navigate to the base directory of the project and open command-prompt / terminal at that location , and enter the below command 
Command to execute : mvn test -Plocal

```




**Report**
```
After the execution is completed , In the base directory of the project , You will find a directory named "TestReports".
In this directory you will find the HTML report named as "Report_<CurrentDate>.html".
```


**Contact :**
```
If you face any issue while using this project , please contact 
* Name: Anish Kumar
* Email: Chatrapati777anish@gmail.com
* Phn: 7338160325
```

