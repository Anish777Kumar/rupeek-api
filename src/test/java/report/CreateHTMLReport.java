package report;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.Multimap;

import tools.Status;


public class CreateHTMLReport {
	
	StringBuffer sb;
	BufferedWriter bw;
	String date;
	File testReportFile ;
	
	
	public CreateHTMLReport() {
		
		sb = new StringBuffer();
		File testDir = new File(System.getProperty("user.dir")+File.separator+"TestReports");
		testDir.mkdirs();
		String PATTERN="dd-MM-yyyy";
		SimpleDateFormat dateFormat=new SimpleDateFormat();
		dateFormat.applyPattern(PATTERN);
		date=dateFormat.format(Calendar.getInstance().getTime());
		
		testReportFile = new File(System.getProperty("user.dir")+File.separator+"TestReports"+File.separator+"Report_"+date+".html");
		
		if(testReportFile.exists())
			testReportFile.delete();
		try {
			testReportFile.createNewFile();
			bw = new BufferedWriter(new FileWriter(testReportFile));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		sb = new StringBuffer();
		
	}
	
	
	public void create(Multimap <String , String>steps ,Map<String,String> executedTests) throws IOException
	{
		System.out.println("==========================STARTED CREATING REPORT=======================================");
		
		sb.append("<!DOCTYPE html>\r\n" + 
				"<html>\n" + 
				"<head>\n" +
				"<title>TEST EXECUTION REPORT : "+date+"</title>"+
				"  <link rel=\"stylesheet\" href=\".."+"File.separator"+"src"+File.separator+"test"+File.separator+"resources"+File.separator+"style.css\">\n" + 
				"<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css\">\r\n" + 
				"<link rel=\"stylesheet\" href=\"..\\src\\test\\resources\\style.css\">"+
				"</head>\n"+
				"<body>");
		
		
		
		Set<String> testCases = executedTests.keySet();
		
		for(String testcase : testCases)
		{
			String status = executedTests.get(testcase);
			List<String> step = (List<String>) steps.get(testcase);
			String color;
			if(status.equalsIgnoreCase(Status.PASSED.toString()))
				color = "green";
			else
				color = "red";
			
			sb.append("<div class=\"container\">\r\n" + 
					"  <h2><font color=\""+color+"\">"+testcase+"--"+status+" </font></h2>\r\n" + 
					"  <ul class=\"responsive-table\">\r\n" + 
					"    <li class=\"table-header\">\r\n" + 
					"      <div class=\"col col-1\">EXPECTED</div>\r\n" + 
					"      <div class=\"col col-2\">ACTUAL</div>\r\n" + 
					"      <div class=\"col col-3\">STATUS</div>\r\n" + 
					"      <div class=\"col col-4\">DURATION(MILLIS)</div>\r\n" + 
					"    </li>");
			
			for(String q : step)
			{
				String arr[] = q.split(Reporter.delimeter);
				sb.append("<li class=\"table-row\">\r\n" + 
						"      <div class=\"col col-1\" data-label=\"EXPECTED\">"+arr[0]+"</div>\r\n" + 
						"      <div class=\"col col-2\" data-label=\"ACTUAL\">"+arr[1]+"</div>\r\n" + 
						"      <div class=\"col col-3\" data-label=\"STATUS\">"+arr[2]+"</div>\r\n" + 
						"      <div class=\"col col-4\" data-label=\"DURATION(MILLIS)\">"+arr[3]+"</div>\r\n" + 
						"    </li>");
			}
			
			sb.append("</ul>\n"
					+ "</div>");
			
		}
		
		
		
		sb.append("</body>\n"+
					"</html>");
		
		
		bw.write(sb.toString());
		bw.flush();
		bw.close();
		
		System.out.println("===================================TEST REPORT HAS BEEN CREATED================================");
		System.out.println("PLEASE NAVIGATE TO THE GIVEN LOCATION TO FIND THE TEST EXECUTION  :"+testReportFile.getPath());
				  
	}
	

}
