package report;

import java.lang.reflect.Method;
import java.util.List;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import tools.BaseTest;

public abstract class Reporter extends BaseTest{

	 String testCaseName = null;
	 public static final String delimeter ="<@SPLIT@>";
	
	
	 
	 @BeforeMethod
	 public void startTest(Method method)
	{
		testCaseName = method.getName();
		System.out.println("The test case started to execute is :"+testCaseName);
	}
	
	
	
	@AfterMethod
	public void endTest()
	{
		boolean flag = true;
		
		List<String> tempStep = (List<String>) BaseTest.steps.get(testCaseName);
		for(String s:tempStep)
		{
			String stepStatus = s.split(delimeter)[2];
			if(stepStatus.equalsIgnoreCase("FAILED"))
			{
				flag=false;
				break;
			}
		}
		
		
		
		if(flag)
		{
			BaseTest.executedTests.put(testCaseName, "PASSED");
		}
		else
		{
			BaseTest.executedTests.put(testCaseName, "FAILED");
		}
		
		System.out.println("Completed test :"+testCaseName);
		testCaseName = null;
		
		
	}
	
	
	public void addStep(String expected, String actual , String status,String duration)
	{
		BaseTest.steps.put(testCaseName, expected+delimeter+actual+delimeter+status+delimeter+duration);
	}
	
}
