package tools;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.json.JSONObject;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import api.payload.Endpoints;
import api.payload.Payload;
import api.payloads.helper.Headers;
import api.payloads.helper.UserModel;
import api.utils.POST;
import common.utils.Logger;
import report.CreateHTMLReport;

public  class BaseTest {

	public static  Multimap <String , String>steps = ArrayListMultimap.create();
	public static  Map<String,String> executedTests = new HashMap<String,String>();
	private static  Properties environmentProperties = new Properties();
	
	private static final Logger log = Logger.getLogger();
	
	@BeforeSuite
	public void beforeSuite() throws IOException
	{
		
		String filepath = System.getProperty("user.dir")+File.separator+"src"+File.separator+"test"+File.separator+"resources"+File.separator+"env.properties";
		InputStream in = new FileInputStream(new File(filepath));
		environmentProperties.load(in);
	}
	
	
	@AfterSuite
	public void afterSuite()
	{
		System.out.println(executedTests);
		System.out.println(steps);
		try {
			new CreateHTMLReport().create(steps, executedTests);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static Object get(String key)
	{
		return environmentProperties.getProperty(key);
	}
	
	public static String getToken()
	{
		Payload payload = new Payload();
		Headers headers = new Headers();
		POST p = new POST(environmentProperties.getProperty("URL"),
							headers.getHeadersForAuthentications(),
							payload.createPayloadForAuthentication(UserModel.get()),
							Endpoints.AUTHENTICATE);
		
		p.execute();
		
		int status = p.getStatus();
		log.info("The status for authentication is :"+status);
		
		if(status==200)
		{
			JSONObject obj =p.getEntityAsJSONObject();
			log.info("The response for authentication is :"+obj);
			return obj.getString("token");
		}
		else
			return null;	
		
	}
	
}
