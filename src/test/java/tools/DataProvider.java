package tools;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import org.testng.ITestContext;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DataProvider {
	
	@org.testng.annotations.DataProvider(name="get-test-data")
	public Iterator<Object[]> getData(Method m ,  ITestContext context)
	{
		ObjectMapper mapper = new ObjectMapper();
		String testCase = m.getName();
		Map<String, Object> testData;
		 try {
			 File f = new File(
			System.getProperty("user.dir")+File.separator+"src"+File.separator+"test"+File.separator+"resources"+File.separator+testCase+".json");
			 testData = mapper.readValue(f, new TypeReference<Map<String, Object>>() {
	            });
	 
	          System.out.println("The test data for test case :"+testCase+" is :"+testData);
	          Collection<Object[]> dp = new ArrayList<Object[]>();
	          dp.add(new Object[]{testData});
			  return dp.iterator();

	 
	        } catch (Exception e) {
	            e.printStackTrace();
	            return null;
	        }
		 
	}

}
