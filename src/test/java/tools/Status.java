package tools;

public enum Status {
	
	PASSED("PASSED"),
	FAILED("FAILED");
	
	String name;
	Status(String s)
	{
		this.name=s;
	}
	
	public String toString()
	{
		return this.name;
	}

}
