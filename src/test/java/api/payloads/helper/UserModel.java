package api.payloads.helper;

import tools.BaseTest;

public class UserModel {
	
	private String user;
	private String password;
	
	private static UserModel model;
	
	private UserModel(String user, String password)
	{
		this.user=user;
		this.password = password;
		
	}

	public String getUser() {
		return user;
	}

	public String getPassword() {
		return password;
	}

	
	public static UserModel get()
	{
		if(model == null)
		{
			synchronized(UserModel.class)
			{
				if(model==null)
				{
					String user = BaseTest.get("username").toString();
					String pwd = BaseTest.get("password").toString();
					 
					model = new UserModel(user, pwd);
				}
			}
		}
		
		return model;
	}
	
}
