package api.payloads.helper;

import java.util.LinkedHashMap;
import java.util.Map;

public class Headers {
	
	public Map<String,String> getHeadersForAuthentications()
	{
		Map<String,String> headers = new LinkedHashMap<String, String>();
		headers.put("Content-Type", "application/json");
		return headers;
	}
	
	public Map<String,String> getHeadersForUsers(String token)
	{
		Map<String,String> headers = new LinkedHashMap<String, String>();
		headers.put("Content-Type", "application/json");
		headers.put("Authorization", "Bearer "+token.trim());
		return headers;
	}

}
