package api.test;

import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import api.payload.Endpoints;
import api.payloads.helper.Headers;
import api.utils.GET;
import common.utils.Logger;
import report.Reporter;
import tools.Status;

public class RupeekAPItest extends Reporter {
	
	Headers headers;
	String baseURL;
	private Logger log;
	
	@BeforeClass
	public void init()
	{
		headers = new Headers();
		baseURL = get("URL").toString();
		log = Logger.getLogger();
		log.info("The base url is :"+baseURL);
	}
	
	
	@Test
	public void TestUsers()
	{
		long start = System.currentTimeMillis();
		String token = getToken();
		GET getUsers = new GET(baseURL, Endpoints.USER, headers.getHeadersForUsers(token));
		getUsers.execute();
		
		int status = getUsers.getStatus();
		if(status == 200)
		{
			addStep("Response status for Users api(/"+Endpoints.USER+"/) should be 200", "Response status for Users api(/"+Endpoints.USER+"/) is :"+status, Status.PASSED.toString(), String.valueOf(System.currentTimeMillis()-start));
		}
		else
		{
			addStep("Response status for Users api(/"+Endpoints.USER+"/) should be 200", "Response status for Users api(/"+Endpoints.USER+"/) is :"+status, Status.FAILED.toString(), String.valueOf(System.currentTimeMillis()-start));
			
		}
		
		start = System.currentTimeMillis();
		
		JSONArray users = getUsers.getEntityAsJSONArray();
		
		if(users!=null && users.length()>0)
		{
			addStep("Response body for Users api(/"+Endpoints.USER+"/) contains users list", "Response body for Users api(/"+Endpoints.USER+"/) contains :"+users.length()+" user details.", Status.PASSED.toString(), String.valueOf(System.currentTimeMillis()-start));	
		}
		else
		{
			addStep("Response body for Users api(/"+Endpoints.USER+"/) contains users list", "Response body for Users api(/"+Endpoints.USER+"/) is either empty or null :"+getUsers.getEntityAsString(), Status.FAILED.toString(), String.valueOf(System.currentTimeMillis()-start));
		}
		
	}
	
	
	@Test
	public void TestUsersWithCorrectMobileNumber()
	{
		
		long start = System.currentTimeMillis();
		String token = getToken();
		GET getUsers = new GET(baseURL, Endpoints.USER, headers.getHeadersForUsers(token));
		getUsers.execute();
		
		int status = getUsers.getStatus();
		if(status == 200)
		{
			addStep("Response status for Users api(/"+Endpoints.USER+"/) should be 200", "Response status for Users api(/"+Endpoints.USER+"/) is :"+String.valueOf(status), Status.PASSED.toString(), String.valueOf(System.currentTimeMillis()-start));
			start= System.currentTimeMillis();
			
			JSONArray users = getUsers.getEntityAsJSONArray();
			
			if(users.length()>0)
			{
				addStep("Response body for Users api(/"+Endpoints.USER+"/) should contains user list", "Response body for Users api(/"+Endpoints.USER+"/) contains "+users.length()+" user details.", Status.PASSED.toString(), String.valueOf(System.currentTimeMillis()-start));
				start = System.currentTimeMillis();
				int randomUserIndex = ThreadLocalRandom.current().nextInt(0, users.length()-1);
				JSONObject user = users.length()>1?users.getJSONObject(randomUserIndex):users.getJSONObject(0);
				
				String phnNumber = user.getString("phone");
				
				GET getIndividualUser = new GET(baseURL, Endpoints.USER, Long.parseLong(phnNumber), headers.getHeadersForUsers(token));
				getIndividualUser.execute();
				
				int statusForIndividualUser = getIndividualUser.getStatus();
				
				if(statusForIndividualUser == 200)
				{
					addStep("Response status for finding individual users using  api should return 200 as status",
							"Response status  for  finding Users with phn number ("+phnNumber+") is "+statusForIndividualUser,
							Status.PASSED.toString(), String.valueOf(System.currentTimeMillis()-start));
					
				}
				else
				{
					addStep("Response status for finding individual users using  phone number should return 200 as status",
							"Response status  for  finding Users with phone number ("+phnNumber+") is "+statusForIndividualUser,
							Status.FAILED.toString(), String.valueOf(System.currentTimeMillis()-start));
				}
					
			
			}
			else
				addStep("Response body for Users api(/"+Endpoints.USER+"/) should contains user list", "Response body for Users api(/"+Endpoints.USER+"/) is empty []", Status.FAILED.toString(), String.valueOf(System.currentTimeMillis()-start));
			
			
			
		}
		else
		{
			addStep("Response status for Users api should be 200", "Response status for Users api is :"+status, Status.FAILED.toString(), String.valueOf(System.currentTimeMillis()-start));
			
		}
		
		
	}
	
	@Test(dataProvider="get-test-data",dataProviderClass=tools.DataProvider.class)
	public void TestUserWithIncorrectMobileNumber(Map testdata)
	{
		long start = System.currentTimeMillis();
		String token = getToken();
		GET getIndividualUser = new GET(baseURL, Endpoints.USER, testdata.get("mobileNumber").toString(), headers.getHeadersForUsers(token));
		getIndividualUser.execute();
		
		int status = getIndividualUser.getStatus();
		log.info("the status for incorrect mobile number search is :"+status);
		
		if(status != 200)
		{
			addStep("Response status for finding individual users using wrong phone number should not return 200 as status",
					"Response status  for  finding Users with wrong phone number ("+testdata.get("mobileNumber")+") is "+status,
					Status.PASSED.toString(), String.valueOf(System.currentTimeMillis()-start));
			
		}
		else
		{
			addStep("Response status for finding individual users using wrong phone number should not return 200 as status",
					"Response status  for  finding Users with wrong phone number ("+testdata.get("mobileNumber")+") is "+status,
					Status.FAILED.toString(), String.valueOf(System.currentTimeMillis()-start));
		}
		
	}
	

}
