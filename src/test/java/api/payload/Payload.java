package api.payload;

import org.json.JSONObject;

import api.payloads.helper.UserModel;

public class Payload {

	
	public String createPayloadForAuthentication(UserModel model) {
		
		JSONObject obj = new JSONObject();
		obj.put("username", model.getUser().trim());
		obj.put("password", model.getPassword().trim());
		
		return obj.toString();
		
	}
	
	
}
