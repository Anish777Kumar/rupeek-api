/**
 * 
 */
package api.payload;

/**
 * @author bunnu
 *
 */
public enum Endpoints {

	
	AUTHENTICATE("authenticate"),
	USER("api/v1/users");
	
	String name;
	Endpoints(String s)
	{
		this.name=s;
	}
	
	public String toString()
	{
		return this.name;
	}
	
	
}
