package api.utils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import api.payload.Endpoints;
import common.utils.Logger;

public class POST {

	HttpResponse response;
	int status;
	String entity;
	JSONObject json_entity;
	JSONArray json_arr;
	String error;
	String baseUrl;
	Endpoints requestEndpoint;
	String payload;
	Map<String,String> headers;
	boolean executed;
	
	private static final Logger log = Logger.getLogger();
	
	
	
	public POST(String baseUrl , Map<String,String> headers , String payload, Endpoints endpoint)
	{
		this.baseUrl=baseUrl.endsWith("/")?baseUrl:baseUrl+"/";
		this.requestEndpoint = endpoint;
		this.headers= headers;
		this.payload=payload;
		this.executed= false;
	}
	
	
	public HttpResponse getResponse() {
		return response;
	}

	private void setResponse(HttpResponse response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	private void setStatus(int status) {
		this.status = status;
	}

	public String getEntityAsString() {
		return entity;
	}

	private void setEntity(String entity) {
		this.entity = entity;
	}

	public JSONObject getEntityAsJSONObject() {
		return json_entity;
	}
	
	public JSONArray getEntityAsJSONArray() {
		return json_arr;
	}

	private void setJson_entity(Object json_entity) {
		
		if(json_entity instanceof JSONObject)
			this.json_entity = (JSONObject) json_entity;
		else if (json_entity instanceof JSONArray)
			this.json_arr = (JSONArray) json_entity;
			
	}

	public String getError() {
		return error;
	}

	private void setError(String error) {
		this.error = error;
	}
	
	public void execute()
	{
		String uri = this.baseUrl+requestEndpoint.toString();
		log.info("The hitting end uri is :"+uri);
		
		HttpPost postRequest = new HttpPost(uri);
		if (headers != null && !headers.isEmpty())
		{
			for (Map.Entry<String, String> entry : headers.entrySet())
				postRequest.setHeader(entry.getKey(), entry.getValue());
		}
		
		postRequest.setEntity(new StringEntity(this.payload, Charset.forName("UTF-8")));
		
		CloseableHttpClient client = HttpClientBuilder.create().build();
		try {
			setResponse(client.execute(postRequest));
			setExecuted(true);
			setStatus(this.response.getStatusLine().getStatusCode());
			setEntity(EntityUtils.toString(this.response.getEntity()));
			
				if(getEntityAsString().trim().startsWith("{"))
					setJson_entity(new JSONObject(getEntityAsString()));
				else if(getEntityAsString().trim().startsWith("["))
					setJson_entity(new JSONArray(getEntityAsString()));
				else 
					setJson_entity(null);
			
			
			
		} catch (Exception e) {
			setError(e.getMessage());
			e.printStackTrace();
		} 
	}


	public boolean isExecuted() {
		return executed;
	}


	private void setExecuted(boolean executed) {
		this.executed = executed;
	}

}
