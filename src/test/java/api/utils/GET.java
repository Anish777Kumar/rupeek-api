package api.utils;

import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import api.payload.Endpoints;
import common.utils.Logger;

 final public class GET {
	
	private HttpResponse response;
	private int status;
	private String entity;
	private JSONObject json_entity;
	private JSONArray json_arr;
	private String error;
	private String baseUrl;
	private Endpoints requestEndpoint;
	private String query;
	private Map<String,String> headers;
	long key;
	boolean executed;
	
	private static final Logger log = Logger.getLogger();
	
	
	public GET(String baseURl,Endpoints endpoint,Map<String,String> headers)
	{
		this.baseUrl=baseURl.endsWith("/")?baseURl:baseURl+"/";
		this.requestEndpoint = endpoint;
		this.headers= headers;
		this.executed=false;
		
	}
	
	public GET(String baseURl,Endpoints endpoint,long idORKey,Map<String,String> headers)
	{
		this.baseUrl=baseURl.endsWith("/")?baseURl:baseURl+"/";
		this.requestEndpoint = endpoint;
		this.headers= headers;
		this.key=idORKey;
		this.executed=false;
	}

	
	public GET(String baseURl,Endpoints endpoint,String query,Map<String,String> headers)
	{
		this.baseUrl=baseURl.endsWith("/")?baseURl:baseURl+"/";
		this.requestEndpoint = endpoint;
		this.headers= headers;
		this.query=query;
		this.executed=false;
	}

	public HttpResponse getResponse() {
		return response;
	}

	private void setResponse(HttpResponse response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	private void setStatus(int status) {
		this.status = status;
	}

	public String getEntityAsString() {
		return entity;
	}

	private void setEntity(String entity) {
		this.entity = entity;
	}

	public JSONObject getEntityAsJSONObject() {
		return json_entity;
	}

	private void setJson_entity(Object json_entity) {
		
		if(json_entity instanceof JSONObject)
			this.json_entity = (JSONObject) json_entity;
		else if (json_entity instanceof JSONArray)
			this.json_arr = (JSONArray) json_entity;
			
	}

	public JSONArray getEntityAsJSONArray() {
		return json_arr;
	}


	public String getError() {
		return error;
	}

	private void setError(String error) {
		this.error = error;
	}
	
	
	public void execute()
	{
		String uri = this.baseUrl+requestEndpoint.toString();
		log.info("The hitting end uri is :"+uri);
		
		if(key!=0)
			uri+=("/"+key);
		
		if(query!=null)
			uri+=("/"+query);
		
		HttpGet getRequest = new HttpGet(uri);
		if (headers != null && !headers.isEmpty())
		{
			for (Map.Entry<String, String> entry : headers.entrySet())
				getRequest.setHeader(entry.getKey(), entry.getValue());
		}
		
		
		CloseableHttpClient client = HttpClientBuilder.create().build();
		try {
			setResponse(client.execute(getRequest));
			setExecuted(true);
			setStatus(this.response.getStatusLine().getStatusCode());
			setEntity(EntityUtils.toString(this.response.getEntity()));
			
				if(getEntityAsString().trim().startsWith("{"))
					setJson_entity(new JSONObject(getEntityAsString()));
				else if(getEntityAsString().trim().startsWith("["))
					setJson_entity(new JSONArray(getEntityAsString()));
				else 
					setJson_entity(null);
			
			
			
		} catch (Exception e) {
			setError(e.getMessage());
			e.printStackTrace();
		} 
	}

	public boolean isExecuted() {
		return executed;
	}

	public void setExecuted(boolean executed) {
		this.executed = executed;
	}
	

}
