package common.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {

	File logFile;
	PrintWriter pw;
    static Logger log =null;
	private Logger()
	{
			logFile=getFile();
			try {
				pw = new PrintWriter(new FileWriter(logFile,true),true);
			}catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
	}
	
	public static  Logger getLogger()
	{
	    if(log==null)
		    {
		        log = new Logger();	
		    }
		return log;
	}
	
	private String getDateTime()
	{
		return new Date().toString();
	}
	
	private String callerMethod()
	{
		return Thread.currentThread().getStackTrace()[3].getMethodName();
	}
	
	private String callerClass()
	{
		String fullyQualifiedClassName= Thread.currentThread().getStackTrace()[3].getClassName();
		String test[] = fullyQualifiedClassName.split("\\.");
		String className= test[test.length-1];
		return className;
	}
	private String callerFile()
	{
		return Thread.currentThread().getStackTrace()[3].getFileName();
	}
	
	private int callerLine()
	{
		return Thread.currentThread().getStackTrace()[3].getLineNumber();
	}
	
	public void info(Object message)
	{
		String actualMessage =this.getDateTime()+" INFO"+" "+this.callerClass()+":"+""+this.callerLine()+" - "+message;
		pw.println(actualMessage);
		System.out.println(actualMessage);
		pw.flush();
	}
	public void error(Object message)
	{
		String actualMessage =this.getDateTime()+" ERROR"+" "+this.callerClass()+":"+""+this.callerLine()+" - "+message;
		pw.println(actualMessage);
		System.err.println(actualMessage);
		pw.flush();
	}
	public void error(String message,Throwable e)
	{
		String actualMessage =this.getDateTime()+" ERROR"+" "+this.callerClass()+":"+""+this.callerLine()+" - "+message;
		pw.println(actualMessage);
		pw.println(e.getMessage());
		System.err.println(actualMessage);
		e.printStackTrace();
		pw.flush();
	}
	public void warn(String message)
	{
		String actualMessage =this.getDateTime()+" WARN"+" "+this.callerClass()+":"+""+this.callerLine()+" - "+message;
		pw.println(actualMessage);
		System.err.println(actualMessage);
		pw.flush();
	}
	
	public void start()
	{
		String actualMessage ="---------------------------------- START @ "+getDateTime()+" -----------------------------------------------------------";
		pw.println(actualMessage);
		System.out.println(actualMessage);
		pw.flush();
	}
	public void end()
	{
		String actualMessage ="---------------------------------- END @ "+getDateTime()+" -----------------------------------------------------------";
		pw.println(actualMessage);
		System.out.println(actualMessage);
		pw.flush();
	}
	
	
	private File getFile()
	{
		 String fileName = "Logs"+new SimpleDateFormat("dd_MM_yyyy").format(new Date())+".log";
		 String location = System.getProperty("user.dir")+File.separator+"logs";
		 File f = new File(location+File.separator+fileName);
		 File l= new File(location);
		 if(!f.exists())
		 {
			 if(!l.exists())
			 {
				 l.mkdirs();
			 }
			 //Creating a file might throw an exception due to permission issues
			 try {
				f.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 }
		 
		 return f;
		 
	}
}
